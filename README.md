# GazpachoSoup

![pipeline](https://gitlab.com/TheWarpedDimension/gazpachosoup/badges/master/pipeline.svg?style=flat)
![coverage](https://gitlab.com/TheWarpedDimension/gazpachosoup/badges/master/coverage.svg?style=flat)

A [souper](https://www.youtube.com/watch?v=3ZGJHDegPcU) attribute-based router for PHP 8.0+. Written from the ground up for interoperability and ease of use, using PHP built-in features instead of workarounds.

## Features

- Fully attribute based, no docblocks
- Controller based
    - Controller convenience methods for outputting data in a structured JSON blob
    - Routes can respond to multiple HTTP Verbs
- Automatic Controller Discovery
    - Multiple controller roots
    - Supports these methods of controller discovery:
        - PSR4 file tree analysis
        - Manual manifest
        - composer.json namespace searching
- Automatic parameter extraction (`$_FILE`, `$_POST`, `$_GET`)
- Regex-based url path parameters
- Extensible authentication handler designed to link in to anything with per-route context
- Router errors are PHP exceptions. Throw as much as you want.
    - Exceptions can have HTTP codes attached.
- Request model parsing
    - Certain classes can be placed in the route parameters and used as-is with no manual parsing.
    - Add your own models to the parser using `IRequestModel`
    - Add other models you don't control using the `HandlerOverrideRegistry`
    - Supports these PHP classes out of the box:
        - DateTime
- Parameter Transformers
    - Modify parameter data as it is being extracted
    - Filter incoming data before it ever reaches the route action for enhanced security
    - Supports these transformers out of the box:
        - Base64Decode (decodes a base64 string)
        - Base64ImageDecode (decodes a base64 image URL into a GDImage, requires ext-gd)
- Error Hooks
    - Intercept errors before they're ever sent to the client and transform them, silence them and more.
    - All stages of the request process are covered.

## Contribute

All contributions are welcome! Just pop over to the repo and submit ideas/changes.