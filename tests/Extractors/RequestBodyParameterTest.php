<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Router;
use WarpedDimension\GazpachoSoup\Tests\Framework\RawController;

class RequestBodyParameterTest extends TestCase
{

    public function testGetRequestBody()
    {
        $_SERVER['REQUEST_URI'] = '/raw/fetchreqbody';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router = new Router([]);
        $router->registerController(new RawController());
        $router->run();
    }
}
