<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Extractors\SessionParameter;

class SessionParameterTest extends TestCase
{
    private const varName = 'testvar';
    private const varValue = 'thisisatest';
    private const badVarName = 'bad';
    private const badVarValue = 'reallybad';

    protected function tearDown(): void
    {
        foreach ( $_SESSION as $k => $v )
        {
            unset($_SESSION[ $k ]);
        }
    }

    public function testIsSet()
    {
        $qp = new SessionParameter();
        $this->assertFalse($qp->isSet(self::varName), sprintf("SessionParameter thinks that %s is set when it isn't.", self::varName));
        $_SESSION[ self::varName ] = self::varValue;
        $this->assertTrue($qp->isSet(self::varName), sprintf("SessionParameter thinks that %s is not set when it is.", self::varName));
    }

    public function testIsSetWithOverride()
    {
        $qp = new SessionParameter(self::varName);
        $_SESSION[ self::badVarName ] = self::badVarValue;
        $this->assertFalse($qp->isSet(self::badVarName), sprintf("SessionParameter thinks that %s is set when it isn't.", self::varName));
        $_SESSION[ self::varName ] = self::varValue;
        unset($_SESSION[ self::badVarName ]);
        $this->assertTrue($qp->isSet(self::badVarName), sprintf("SessionParameter thinks that %s is not set when it is.", self::varName));
    }

    public function testGetParameterValue()
    {
        $qp = new SessionParameter();
        $this->assertNull($qp->getParameterValue(self::varName), sprintf("SessionParameter thinks that %s is set when it isn't.", self::varName));
        $_SESSION[ self::varName ] = self::varValue;
        $this->assertEquals(self::varValue, $qp->getParameterValue(self::varName), "QueryParameter got invalid value.");
    }

    public function testGetParameterValueWithOverride()
    {
        $qp = new SessionParameter(self::varName);
        $_SESSION[ self::badVarName ] = self::badVarValue;
        $this->assertNull($qp->getParameterValue(self::badVarName), sprintf("QueryParameter thinks that %s is set when it isn't.", self::varName));
        $_SESSION[ self::varName ] = self::varValue;
        $this->assertEquals(self::varValue, $qp->getParameterValue(self::badVarName), "QueryParameter got invalid value.");
    }

}
