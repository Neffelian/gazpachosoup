<?php

namespace WarpedDimension\GazpachoSoup\Tests\Transformers;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Transformers\Base64Decode;

class Base64DecodeTest extends TestCase
{

    private function tempFunc( string $test )
    {
        //this is a temp function to obtain a fake parameter to pass into the transform functions.
    }

    public function testPre()
    {
        $decoder = new Base64Decode();
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod("tempFunc")->getParameters()[0];
        $this->assertEquals("iamatest", $decoder->transformPre($param, "aWFtYXRlc3Q="));
    }

    public function testPost()
    {
        $decoder = new Base64Decode();
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod("tempFunc")->getParameters()[0];
        $this->assertEquals("iamatest", $decoder->transformPost($param, "iamatest"));
    }

}
