<?php

namespace WarpedDimension\GazpachoSoup\Tests\Transformers;

use GdImage;
use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Transformers\Base64ImageDecode;

class Base64ImageDecodeTest extends TestCase
{

    private const image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAG1BMVEXMzMyWlpbFxcWqqqqjo6O+vr6cnJy3t7exsbFqQrZYAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAN0lEQVQImWNgIAswGSswMKgxGTOwBAEZhSxBDGxuCu0BjGxuDIyhBSoMjIyhDBzGIBEOY5IMBgCTRQYtXy/reQAAAABJRU5ErkJggg==";

    private function tempFunc( string $test )
    {
        //this is a temp function to obtain a fake parameter to pass into the transform functions.
    }

    public function testPre()
    {
        $decoder = new Base64ImageDecode();
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod("tempFunc")->getParameters()[0];
        /** @var GdImage $image */
        $image = $decoder->transformPre($param, self::image);
        $this->assertInstanceOf(GdImage::class, $image, "Returned invalid image.");
        imagedestroy($image);
    }

    public function testPreBadImage()
    {
        $decoder = new Base64ImageDecode();
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod("tempFunc")->getParameters()[0];
        /** @var GdImage $image */
        $image = $decoder->transformPre($param, str_replace('iVBOR', 'nope', self::image));
        $this->assertNotInstanceOf(GdImage::class, $image, "Returned invalid image.");
        $this->assertFalse($image, 'Returned not false.');
    }

    public function testPost()
    {
        $decoder = new Base64ImageDecode();
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod("tempFunc")->getParameters()[0];
        $this->assertEquals("iamatest", $decoder->transformPost($param, "iamatest"));
    }

}
