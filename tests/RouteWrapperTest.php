<?php

namespace WarpedDimension\GazpachoSoup\Tests;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Exceptions\BadParameterException;
use WarpedDimension\GazpachoSoup\Exceptions\MissingRequiredParameterException;
use WarpedDimension\GazpachoSoup\Extractors\QueryParameter;
use WarpedDimension\GazpachoSoup\HttpVerbs;
use WarpedDimension\GazpachoSoup\Index;
use WarpedDimension\GazpachoSoup\Route;
use WarpedDimension\GazpachoSoup\RouteWrapper;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestController;

class RouteWrapperTest extends TestCase
{

    private static RouteWrapper $wrapper;
    private static RouteWrapper $wrapperUnauthenticated;
    private static RouteWrapper $wrapperIndex;

    public static function setUpBeforeClass(): void
    {
        $controller = new TestController();
        self::$wrapper = new RouteWrapper($controller, (new \ReflectionClass(TestController::class))->getMethod('testRoute'));
        self::$wrapperUnauthenticated = new RouteWrapper($controller, (new \ReflectionClass(TestController::class))->getMethod('testRouteUnauthenticated'));
        self::$wrapperIndex = new RouteWrapper($controller, (new \ReflectionClass(TestController::class))->getMethod('testIndex'));
    }

    protected function setUp(): void
    {
        foreach ( $_GET as $k => $v )
        {
            unset($_GET[ $k ]);
        }
        foreach ( $_POST as $k => $v )
        {
            unset($_POST[ $k ]);
        }
    }

    public function testConstruct()
    {
        $controller = new TestController();
        $method = (new \ReflectionClass(TestController::class))->getMethod('testRoute');
        $wrapper = new RouteWrapper($controller, $method);
        $this->assertEquals($controller, $wrapper->getController());
        $this->assertEquals($method, $wrapper->getMethod());
    }

    public function testGetPathParameterIndex()
    {
        $param1 = self::$wrapper->getMethod()->getParameters()[0];
        $param2 = self::$wrapper->getMethod()->getParameters()[1];
        $param3 = self::$wrapper->getMethod()->getParameters()[2];
        $param4 = self::$wrapper->getMethod()->getParameters()[3];
        $param5 = self::$wrapper->getMethod()->getParameters()[4];
        $this->assertEquals(0, self::$wrapper->getPathParameterIndex($param1));
        $this->assertEquals(false, self::$wrapper->getPathParameterIndex($param2));
        $this->assertEquals(false, self::$wrapper->getPathParameterIndex($param3));
        $this->assertEquals(false, self::$wrapper->getPathParameterIndex($param4));
        //Because param2 is a QueryParameter param3 is a PostParameter, param4 is a MethodParameter, param5 should be moved down to 1.
        $this->assertEquals(1, self::$wrapper->getPathParameterIndex($param5));
    }

    public function testGetAuthenticatedAttribute()
    {
        $this->assertNull(self::$wrapperUnauthenticated->getAuthenticatedAttribute());
        $attr = self::$wrapper->getAuthenticatedAttribute();
        $this->assertNotNull($attr);
        $this->assertInstanceOf(Authenticated::class, $attr);
    }

    public function testIsAuthenticated()
    {
        $this->assertTrue(self::$wrapper->isAuthenticated());
        $this->assertFalse(self::$wrapperUnauthenticated->isAuthenticated());
    }

    public function testGetPathParameters()
    {
        $params = self::$wrapper->getPathParameters();

        $expected = [
            "test1" => [
                "name" => "test1",
                "type" => null,
                "optional" => false,
                "default" => null
            ],
            "test5" => [
                "name" => "test5",
                "type" => "string",
                "optional" => true,
                "default" => null
            ]
        ];

        foreach ( $expected as $name => $value )
        {
            $data = $params[ $name ];
            $this->assertNotNull($data, sprintf("Data for expected parameter %s was not found in getPathParameters output", $value['name']));
            $this->assertEquals($value['name'], $data['name'], sprintf("Name does not match for param %s", $value['name']));
            $this->assertEquals($value['optional'], $data['optional'], sprintf("Optional does not match for param %s", $value['name']));
            $this->assertEquals($value['default'], $data['default'], sprintf("Default does not match for param %s", $value['name']));
            if ( $data['type'] !== null )
            {
                $this->assertEquals($value['type'], $data['type']->getName(), sprintf("Type does not match for param %s", $value['name']));
            }
        }

        $this->assertFalse(isset($params['test2']), "IParameterExtractor was included in path parameters.");
        $this->assertFalse(isset($params['test3']), "IParameterExtractor was included in path parameters.");
        $this->assertFalse(isset($params['test4']), "MethodParameter was included in path parameters.");
    }

    public function testGetParametersWithExtractors()
    {
        $params = self::$wrapper->getParametersWithExtractors();
        $paramsOnlyGet = self::$wrapper->getParametersWithExtractors(QueryParameter::class);

        $this->assertCount(2, $params);
        $this->assertCount(1, $paramsOnlyGet);
    }

    public function testGetRouteAttribute()
    {
        $attr = self::$wrapper->getRouteAttribute();
        $this->assertNotNull($attr, "Route is null.");
        $this->assertInstanceOf(Route::class, $attr, "Route is not instance of Route.");

        $attr = self::$wrapperIndex->getRouteAttribute();
        $this->assertNotNull($attr, "Index is null");
        $this->assertInstanceOf(Index::class, $attr, "Route is not instance of Index.");
    }

    public function testInvoke()
    {
        $path = [
            "test1234"
        ];
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = "1234";
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokeMissingPathParameter()
    {
        $path = [
        ];
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = "1234";
        $this->expectException(MissingRequiredParameterException::class);
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokeMissingGetParameter()
    {
        $path = [
            "test1234"
        ];
        //$_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = "1234";
        $this->expectException(MissingRequiredParameterException::class);
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokeMissingPostParameter()
    {
        $path = [
            "test1234"
        ];
        $_GET['test2'] = base64_encode("Hello World");
        //$_POST['test3'] = "1234";
        $this->expectException(MissingRequiredParameterException::class);
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokeWithOptionalPathParameter()
    {
        $path = [
            "test1234",
            "OptionalPath"
        ];
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = "1234";
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokeBadType()
    {
        $path = [
            "test1234",
            "OptionalPath"
        ];
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = "nope";
        $this->expectException(BadParameterException::class);
        $this->expectExceptionMessage("Parameter 'test3' is of wrong type 'string'. Expected type 'int' instead.");
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }

    public function testInvokePassNullToNonOptional()
    {
        $path = [
            "test1234",
            "OptionalPath"
        ];
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = null;
        $this->expectException(BadParameterException::class);
        self::$wrapper->invoke($path, HttpVerbs::VERB_POST);
    }
}
