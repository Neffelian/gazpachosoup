<?php

namespace WarpedDimension\GazpachoSoup\Tests;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Finders\PSR4ControllerFinder;
use WarpedDimension\GazpachoSoup\Router;
use WarpedDimension\GazpachoSoup\Tests\Framework\AuthTest;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestController;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestErrorHandler;

class RouterTest extends TestCase
{

    public function testFinders()
    {
        $router = new Router([
            new PSR4ControllerFinder(__DIR__ . '/Framework', '\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\')
        ]);
        $controllers = $router->getControllers();
        $this->assertCount(3, $controllers);
    }

    public function testAuthentication()
    {
        $router = new Router([]);
        $router->registerController(new TestController());
        $router->setAuthenticationHandler(new AuthTest(true));
        $_SERVER['REQUEST_URI'] = '/test/testRoute/test1234';
        $_GET['test2'] = base64_encode("Hello World");
        $_POST['test3'] = 1234;
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router->run();

        $_SERVER['REQUEST_URI'] = '/test/testRouteUnauthenticated';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $router->run();
    }

    public function testException()
    {
        $router = new Router([]);
        $router->registerController(new TestController());
        $errorHandler = new TestErrorHandler();
        $router->setErrorHandler($errorHandler);
        $_SERVER['REQUEST_URI'] = '/test/testException';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $router->run();
        $this->assertTrue($errorHandler->isGeneric());
    }

    public function test401()
    {
        $router = new Router([]);
        $router->registerController(new TestController());
        $router->setAuthenticationHandler(new AuthTest(false));
        $errorHandler = new TestErrorHandler();
        $router->setErrorHandler($errorHandler);
        $_SERVER['REQUEST_URI'] = '/test/testRoute/test1234';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router->run();
        $this->assertTrue($errorHandler->isForbidden());
    }

    public function test404()
    {
        $router = new Router([]);
        $router->registerController(new TestController());
        $errorHandler = new TestErrorHandler();
        $router->setErrorHandler($errorHandler);
        $_SERVER['REQUEST_URI'] = '/testing123';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $router->run();
        $this->assertTrue($errorHandler->isNotFound());
    }

    public function testBadMethod()
    {
        $router = new Router([]);
        $router->registerController(new TestController());
        $errorHandler = new TestErrorHandler();
        $router->setErrorHandler($errorHandler);
        $_SERVER['REQUEST_URI'] = '/test/testRouteUnauthenticated';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router->run();
        $this->assertTrue($errorHandler->isMethodNotAllowed());
    }

}
