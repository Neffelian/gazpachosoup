<?php

namespace WarpedDimension\GazpachoSoup\Tests\Results;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Finders\PSR4ControllerFinder;
use WarpedDimension\GazpachoSoup\Router;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestResultTransformer;

class ResultTransformerTest extends TestCase
{
    public function testTransformer()
    {
        $router = new Router([
            new PSR4ControllerFinder(__DIR__ . '/../Framework', '\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\')
        ]);
        $router->registerResultTransformer(new TestResultTransformer);
        $_SERVER['REQUEST_URI'] = '/test/index';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        ob_start();
        $router->run();
        $result = json_decode(ob_get_clean(), true);
        $this->assertEquals("thisisatest", $result['testheader']);
    }

}
