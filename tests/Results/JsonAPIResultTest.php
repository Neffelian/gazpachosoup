<?php

namespace WarpedDimension\GazpachoSoup\Tests\Results;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Results\JsonAPIResult;

class JsonAPIResultTest extends TestCase
{

    public function testGetSetHttpCode()
    {
        $result = new JsonAPIResult(200, [], [], null, false);
        $result->setHttpCode(201);
        $this->assertEquals(201, $result->getHttpCode());
    }

    public function testSetGetHeader()
    {
        $result = new JsonAPIResult(200, [], [], null, false);
        $result->setHeader("test123", "testvalue");
        $this->assertEquals("testvalue", $result->getHeader("test123"));
    }

    public function testGetHeaders()
    {
        $result = new JsonAPIResult(200, [], [ "test123" => "testvalue", "test456" => "valuetest" ], null, false);
        $headers = $result->getHeaders();
        $this->assertCount(2, $headers);
        $this->assertEquals("testvalue", $headers["test123"]);
        $this->assertEquals("valuetest", $headers["test456"]);
    }

    public function testRemoveHeader()
    {
        $result = new JsonAPIResult(200, [], [ "test123" => "testvalue" ], null, false);
        $this->assertCount(1, $result->getHeaders());
        $result->removeHeader("test123");
        $this->assertCount(0, $result->getHeaders());
    }

    public function testJsonSerialize()
    {
        $result = new JsonAPIResult(200, [ "test123" => false ], [ "test123" => "testvalue", "test456" => "valuetest" ], "this is a message", true);
        $expected = [
            'code' => 200,
            'error' => true,
            'message' => "this is a message",
            'test123' => "testvalue",
            'test456' => "valuetest"
        ];
        $result = $result->jsonSerialize();
        foreach ( $expected as $key => $value )
        {
            $this->assertEquals($value, $result[ $key ], "$key does not match expected value. Was: $result[$key]. Expected: $value.");
        }
        $this->assertCount(1, $result['body']);
        $this->assertEquals(false, $result['body']['test123']);
    }

    public function testGetBody()
    {
        $result = new JsonAPIResult(200, [ "test123" => false ], [ "test123" => "testvalue", "test456" => "valuetest" ], "this is a message", true);
        $this->assertEquals([ "test123" => false ], $result->getBody());
    }

    public function testGetSetCode()
    {
        $result = new JsonAPIResult(200, [ "test123" => false ], [ "test123" => "testvalue", "test456" => "valuetest" ], "this is a message", true);
        $this->assertEquals(200, $result->getCode());
        $result->setCode(201);
        $this->assertEquals(201, $result->getCode());
    }

    public function testClearHeaders()
    {
        $result = new JsonAPIResult(200, [ "test123" => false ], [ "test123" => "testvalue", "test456" => "valuetest" ], "this is a message", true);
        $result->clearHeaders();
        $this->assertCount(0, $result->getHeaders());
    }

    public function testSetBody()
    {
        $result = new JsonAPIResult(200, [ "test123" => false ], [ "test123" => "testvalue", "test456" => "valuetest" ], "this is a message", true);
        $result->setBody([ "thisisatest" => "yesthisisatest" ]);
        $this->assertCount(1, $result->getBody());
        $this->assertEquals("yesthisisatest", $result->getBody()["thisisatest"]);
    }
}
