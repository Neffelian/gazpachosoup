<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverride;

class TestingDateTimeZoneOverride
{

    #[HandlerOverride(\DateTimeZone::class)]
    public static function convert( \ReflectionParameter $parameter, mixed $input ): mixed
    {
        return "obvious test data";
    }

}