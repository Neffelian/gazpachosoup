<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use Attribute;
use WarpedDimension\GazpachoSoup\Extractors\RequestBodyParameter;

#[Attribute(Attribute::TARGET_PARAMETER)]
class MockableRequestBodyParameter extends RequestBodyParameter
{

    protected static function getRequestBodyData(): string
    {
        return json_encode([
            "body_item_1" => "body_item_1_value",
            "body_item_2" => "body_item_2_value"
        ]);
    }

}