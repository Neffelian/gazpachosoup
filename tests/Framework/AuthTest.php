<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use PHPUnit\Framework\Assert;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Authentication\IAuthenticationHandler;
use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\RouteWrapper;

class AuthTest implements IAuthenticationHandler
{

    private bool $response;

    public function __construct( bool $response )
    {
        $this->response = $response;
    }

    /**
     * @inheritDoc
     */
    function checkAuthentication( ControllerBase $controller, ?RouteWrapper $route, ?Authenticated $authenticationContext ): bool
    {
        echo "Checking fake auth...\n";
        echo "Controller: " . get_class($controller) . "\n";
        echo "Route: " . ($route?->getMethod()->getName() ?? "None, checking controller only") . "\n";
        Assert::assertInstanceOf(TestController::class, $controller);
        if ( $route !== null )
            Assert::assertEquals('testRoute', $route->getMethod()->getName(), 'Checking authentication of nont authenticated route');
        return $this->response;
    }
}