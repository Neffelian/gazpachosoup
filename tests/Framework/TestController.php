<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use PHPUnit\Framework\Assert;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Controller;
use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\Exceptions\GazpachoSoupException;
use WarpedDimension\GazpachoSoup\Extractors\MethodParameter;
use WarpedDimension\GazpachoSoup\Extractors\PostParameter;
use WarpedDimension\GazpachoSoup\Extractors\QueryParameter;
use WarpedDimension\GazpachoSoup\HttpVerbs;
use WarpedDimension\GazpachoSoup\Index;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Route;
use WarpedDimension\GazpachoSoup\Transformers\Base64Decode;

#[Controller]
#[Authenticated]
#[Route('/test')]
class TestController extends ControllerBase
{

    #[Authenticated, Route('/testRoute/(.+)', HttpVerbs::VERB_POST)]
    public function testRoute( $test1, #[QueryParameter, Base64Decode] $test2, #[PostParameter] int $test3, #[MethodParameter] $test4, string $test5 = null ): IResult
    {
        Assert::assertEquals("test1234", $test1);
        Assert::assertEquals("Hello World", $test2);
        Assert::assertEquals(1234, $test3);
        Assert::assertEquals(HttpVerbs::VERB_POST, $test4);
        if ( $test5 !== null )
        {
            Assert::assertEquals("OptionalPath", $test5);
        }
        return self::JsonAPISuccess();
    }

    #[Route('/testRouteUnauthenticated')]
    public function testRouteUnauthenticated(): IResult
    {
        return self::JsonAPISuccess();
    }

    #[Index]
    public function testIndex(): IResult
    {
        return self::JsonAPISuccess();
    }

    #[Route('/testException')]
    public function testThrowException(): IResult
    {
        throw new GazpachoSoupException();
    }

}