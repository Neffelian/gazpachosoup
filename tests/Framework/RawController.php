<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use PHPUnit\Framework\Assert;
use WarpedDimension\GazpachoSoup\Controller;
use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\HttpVerbs;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Route;

#[Controller]
#[Route('/raw')]
class RawController extends ControllerBase
{

    #[Route('/fetchreqbody', HttpVerbs::VERB_POST)]
    public function fetchReqBody( #[MockableRequestBodyParameter] array $body ): IResult
    {
        Assert::assertEquals([
            "body_item_1" => "body_item_1_value",
            "body_item_2" => "body_item_2_value"
        ], $body);
        return self::JsonAPISuccess();
    }

    #[Route('/fetch_raw_param', HttpVerbs::VERB_POST)]
    public function fetch_raw_param( #[MockableRawParameter] string $body_item_1 ): IResult
    {
        Assert::assertEquals("body_item_1_value", $body_item_1);
        return self::JsonAPISuccess();
    }

    #[Route('/fetch_raw_param/no_exist', HttpVerbs::VERB_POST)]
    public function fetch_raw_param_does_not_exist( #[MockableRawParameter] string $body_item_3 ): IResult
    {
        return self::JsonAPISuccess();
    }

}