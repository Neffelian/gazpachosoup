<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverride;
use WarpedDimension\GazpachoSoup\RouteWrapper;

class BadOverrides
{

    #[HandlerOverride(\DateTime::class)]
    public static function badOverrideAlreadyExists( \ReflectionParameter $parameter, mixed $value ): mixed
    {
        return null;
    }

    public static function badOverrideNoAttribute( \ReflectionParameter $parameter, mixed $value ): mixed
    {
        return null;
    }

    #[HandlerOverride(RouteWrapper::class)]
    public function badOverrideNonStatic( \ReflectionParameter $parameter, mixed $value ): mixed
    {
        return null;
    }

}