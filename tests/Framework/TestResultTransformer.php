<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use WarpedDimension\GazpachoSoup\Results\IMutableResult;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Results\IResultTransformer;

class TestResultTransformer implements IResultTransformer
{

    function transform( IMutableResult $result ): IResult
    {
        $result->setHeader("testheader", "thisisatest");
        return $result;
    }
}