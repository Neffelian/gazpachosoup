<?php

namespace WarpedDimension\GazpachoSoup\Tests\ModelParser;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverride;

class HandlerOverrideTest extends TestCase
{

    public function testGetTargetClassName()
    {
        $ho = new HandlerOverride("testing");
        $this->assertEquals("testing", $ho->getTargetClassName());
    }
}
