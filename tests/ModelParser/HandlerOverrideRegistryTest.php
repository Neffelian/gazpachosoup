<?php

namespace WarpedDimension\GazpachoSoup\Tests\ModelParser;

use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverrideRegistry;
use WarpedDimension\GazpachoSoup\ModelParser\NoRegisteredHandlerOverrideException;
use WarpedDimension\GazpachoSoup\RouteWrapper;
use WarpedDimension\GazpachoSoup\Tests\Framework\BadOverrides;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestingDateTimeZoneOverride;

class HandlerOverrideRegistryTest extends TestCase
{

    private function templateFuncBlank( DateTimeZone $dtz )
    {
        //template function to get a nicely generated parameter for testing.
    }

    public function testAutoloadedOverrides()
    {
        $types = [
            DateTime::class
        ];
        foreach ( $types as $type )
        {
            $this->assertTrue(HandlerOverrideRegistry::hasHandler($type));
        }
    }

    public function testConvertUnregisteredType()
    {
        $this->expectException(NoRegisteredHandlerOverrideException::class);
        $rc = new ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncBlank')->getParameters()[0];
        HandlerOverrideRegistry::attemptConversion(DateTimeZone::class, $param, "doesnotmatter");
    }

    public function testNotLoadedUnknown()
    {
        $this->assertFalse(HandlerOverrideRegistry::hasHandler(DateTimeZone::class));
    }

    public function testLoadNewHandler()
    {
        $this->assertFalse(HandlerOverrideRegistry::hasHandler(DateTimeZone::class));
        HandlerOverrideRegistry::registerHandlersInClass(TestingDateTimeZoneOverride::class);
        $this->assertTrue(HandlerOverrideRegistry::hasHandler(DateTimeZone::class));
    }

    /**
     * @depends testLoadNewHandler
     */
    public function testConvertUsingNewHandler()
    {
        $rc = new ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncBlank')->getParameters()[0];
        $result = HandlerOverrideRegistry::attemptConversion(DateTimeZone::class, $param, "doesnotmatter");
        $this->assertEquals("obvious test data", $result);
    }

    public function testNotLoadingBadHandlers()
    {
        HandlerOverrideRegistry::registerHandlersInClass(BadOverrides::class);
        $this->assertFalse(HandlerOverrideRegistry::hasHandler(RouteWrapper::class));
    }

}
