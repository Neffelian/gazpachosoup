<?php

namespace WarpedDimension\GazpachoSoup\Tests;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Controller;
use WarpedDimension\GazpachoSoup\Tests\Framework\SubDirectoryTest\TestController2;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestController;

class ControllerBaseTest extends TestCase
{

    public function testGetAuthenticatedAttribute()
    {
        $controller = new TestController();
        $attr = $controller->getAuthenticatedAttribute();
        $this->assertInstanceOf(Authenticated::class, $attr);
        $controller = new TestController2();
        $attr = $controller->getAuthenticatedAttribute();
        $this->assertNull($attr);
    }

    public function testGetAllRoutes()
    {
        $controller = new TestController();
        $routes = $controller->getAllRoutes();
        $this->assertCount(4, $routes);
        $names = [ "testRoute", "testRouteUnauthenticated", "testIndex" ];
        foreach ( $names as $name )
        {
            $this->assertCount(1, array_filter($routes, function( $r ) use ( $name ) {
                return $r->getMethod()->getName() === $name;
            }));
        }
    }

    public function testGetMetadataAttribute()
    {
        $controller = new TestController();
        $attr = $controller->getMetadataAttribute();
        $this->assertInstanceOf(Controller::class, $attr);
    }

    public function testGetRouteAttribute()
    {
        $controller = new TestController();
        $attr = $controller->getRouteAttribute();
        $this->assertEquals("/test", $attr->path);
        $controller = new TestController2();
        $attr = $controller->getRouteAttribute();
        $this->assertNull($attr);
    }
}
