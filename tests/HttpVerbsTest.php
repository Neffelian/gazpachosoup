<?php

namespace WarpedDimension\GazpachoSoup\Tests;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\HttpVerbs;

class HttpVerbsTest extends TestCase
{

    public function testGetFromString()
    {
        $strings = [
            "get" => HttpVerbs::VERB_GET,
            "head" => HttpVerbs::VERB_HEAD,
            "post" => HttpVerbs::VERB_POST,
            "put" => HttpVerbs::VERB_PUT,
            "delete" => HttpVerbs::VERB_DELETE,
            "trace" => HttpVerbs::VERB_TRACE,
            "options" => HttpVerbs::VERB_OPTIONS,
            "connect" => HttpVerbs::VERB_CONNECT,
            "patch" => HttpVerbs::VERB_PATCH
        ];
        foreach ( $strings as $string => $expected )
        {
            $this->assertEquals($expected, HttpVerbs::getFromString($string));
        }
    }

    public function testGetCurrent()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $this->assertEquals(HttpVerbs::VERB_GET, HttpVerbs::getCurrent());
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $this->assertEquals(HttpVerbs::VERB_POST, HttpVerbs::getCurrent());
    }
}
