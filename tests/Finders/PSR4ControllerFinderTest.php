<?php

namespace WarpedDimension\GazpachoSoup\Tests\Finders;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Exceptions\BadNamespaceException;
use WarpedDimension\GazpachoSoup\Exceptions\FileNotFoundException;
use WarpedDimension\GazpachoSoup\Finders\PSR4ControllerFinder;
use WarpedDimension\GazpachoSoup\Tests\Framework\SubDirectoryTest\TestController2;
use WarpedDimension\GazpachoSoup\Tests\Framework\TestController;

class PSR4ControllerFinderTest extends TestCase
{

    public function testFindController()
    {
        $finder = new PSR4ControllerFinder(__DIR__ . '/../Framework', '\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\', false);
        $controllers = $finder->getControllerNamespaces();
        $this->assertNotCount(0, $controllers, "No controllers were found.");
        $this->assertCount(1, array_filter($controllers, function( $var ) {
            return $var === "\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\TestController";
        }), TestController::class . " was not found.");
    }

    public function testFindControllerRecursive()
    {
        $finder = new PSR4ControllerFinder(__DIR__ . '/../Framework', '\\WarpedDimension\\GazpachoSoup\\Tests\\Framework', true);
        $controllers = $finder->getControllerNamespaces();
        $this->assertNotCount(0, $controllers, "No controllers were found.");
        $this->assertCount(1, array_filter($controllers, function( $var ) {
            return $var === "\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\TestController";
        }), TestController::class . " was not found.");
        $this->assertCount(1, array_filter($controllers, function( $var ) {
            return $var === "\\WarpedDimension\\GazpachoSoup\\Tests\\Framework\\SubDirectoryTest\\TestController2";
        }), TestController2::class . " was not found.");
    }

    public function testControllerDirectoryDoesNotExist()
    {
        $this->expectException(FileNotFoundException::class);
        $finder = new PSR4ControllerFinder(__DIR__ . '/FrameworkNotExist', '\\WarpedDimension\\GazpachoSoup\\Tests\\Finders\\FrameworkNotExist');
    }

    public function testControllerNamespaceMismatch()
    {
        $this->expectException(BadNamespaceException::class);
        $finder = new PSR4ControllerFinder(__DIR__ . '/../Framework', '\\WarpedDimension\\GazpachoSoup\\Tests', true);
        $controllers = $finder->getControllerNamespaces();
    }

}
