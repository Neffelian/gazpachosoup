FROM composer:latest as composer
FROM php:8.0-apache
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN apt-get update -y
RUN apt-get install -y mariadb-client unzip
RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions gd xdebug mcrypt mysqli mbstring zip xml sodium imap
COPY --from=composer  /usr/bin/composer /usr/bin/composer
COPY setup/config/apache/apache2.conf /etc/apache2/apache2.conf
COPY setup/config/apache/sites/*.conf /etc/apache2/sites-available/
RUN a2dissite 000-default
RUN rm /etc/apache2/sites-available/000-default.conf
RUN rm /etc/apache2/sites-available/default-ssl.conf
RUN a2ensite default-http default-https
RUN a2enmod rewrite ssl