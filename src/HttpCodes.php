<?php

namespace WarpedDimension\GazpachoSoup;

/**
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 * @codeCoverageIgnore
 */
abstract class HttpCodes
{

    /**
     * Success response
     */
    const CODE_SUCCESS = 200;

    /**
     * Client screwed up generally
     */
    const CODE_CLIENT_ERROR = 400;

    /**
     * Client hasn't authenticated yet
     */
    const CODE_UNAUTHORISED = 401;

    /**
     * Client has not got permission to view resource
     */
    const CODE_FORBIDDEN = 403;

    /**
     * Client requested a resource that doesn't exist
     */
    const CODE_NOT_FOUND = 404;

    /**
     * Client made a request with the wrong verb.
     */
    const CODE_METHOD_NOT_ALLOWED = 405;

    /**
     * Server screwed up generally.
     */
    const CODE_SERVER_ERROR = 500;

}