<?php

namespace WarpedDimension\GazpachoSoup\Finders;

use HaydenPierce\ClassFinder\ClassFinder;
use WarpedDimension\GazpachoSoup\ControllerBase;

/**
 * Finds controllers in a namespace using {@see ClassFinder}
 *
 * @package WarpedDimension\GazpachoSoup\Finders
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class HaydenControllerFinder implements IControllerFinder
{

    private string $baseNamespace;

    public function __construct( string $baseNamespace )
    {
        $this->baseNamespace = $baseNamespace;
    }

    public function getControllerNamespaces(): array
    {
        ClassFinder::disablePSR4Support();
        ClassFinder::enableClassmapSupport();
        $classes = ClassFinder::getClassesInNamespace($this->baseNamespace, ClassFinder::RECURSIVE_MODE);
        $return = [];
        foreach ( $classes as $class )
        {
            if ( is_a($class, ControllerBase::class, true) )
                $return[] = $class;
        }
        return $return;
    }
}