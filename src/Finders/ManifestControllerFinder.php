<?php

namespace WarpedDimension\GazpachoSoup\Finders;

/**
 * Finds controllers using a manifest.json in the controller root.
 *
 * @package WarpedDimension\GazpachoSoup\Finders
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class ManifestControllerFinder implements IControllerFinder
{

    private string $controllerFolder;

    /**
     * ManifestControllerFinder constructor.
     *
     * @param string $controllerFolder the path that we'll find the controllers and "manifest.json" in.
     */
    public function __construct( string $controllerFolder )
    {
        $this->controllerFolder = realpath($controllerFolder);
    }

    public function loadManifest(): array
    {
        return json_decode(file_get_contents($this->controllerFolder . '/manifest.json'), true);
    }

    public function getControllerNamespaces(): array
    {
        $manifest = $this->loadManifest();
        $baseNamespace = $manifest['namespace_root'];
        if ( !str_ends_with($baseNamespace, '\\') )
            $baseNamespace .= '\\';
        $controllers = $manifest['controllers'];
        $namespaces = [];
        foreach ( $controllers as $controller )
            $namespaces[] = $baseNamespace . $controller;
        return $namespaces;
    }

}