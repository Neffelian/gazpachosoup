<?php

namespace WarpedDimension\GazpachoSoup\Finders;

use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\Exceptions\BadNamespaceException;
use WarpedDimension\GazpachoSoup\Exceptions\FileNotFoundException;

/**
 * Finds controllers by scanning directories in a PSR-4 directory structure.
 * Note: probably shouldn't use this on prod unless you've got robust write protection to the controller directory.
 *
 * @package WarpedDimension\GazpachoSoup\Finders
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class PSR4ControllerFinder implements IControllerFinder
{

    /**
     * A list of paths that we don't want to consider for finding controllers.
     */
    const ILLEGAL_PATHS = [ '.', '..' ];

    /**
     * @var string The root namespace for the controllers.
     */
    private string $rootNamespace;

    /**
     * @var string The folder that the controllers live in. TODO: compute this using the composer.json of the host project.
     */
    private string $controllerFolder;

    /**
     * @var bool Should we look for controllers recursively through the structure?
     */
    private bool $recursive;

    /**
     * ControllerFinder constructor.
     *
     * @param string $controllerFolder the folder that we'll find the controllers in
     * @param string $rootNamespace    the namespace prefix for this folder
     * @param bool   $recursive        Should we look for controllers recursively through the structure?
     *
     * @throws FileNotFoundException if the {@see $controllerFolder} doesn't exist
     */
    public function __construct( string $controllerFolder, string $rootNamespace, bool $recursive = true )
    {
        //For ease of use, the namespace must always end with a trailing \ so we can just append onto it.
        if ( !str_ends_with($rootNamespace, '\\') )
            $rootNamespace .= '\\';
        $this->rootNamespace = $rootNamespace;
        $fullPath = realpath($controllerFolder);
        if ( $fullPath === false ) //If realpath failed, then the directory doesn't exist.
            throw new FileNotFoundException(null, $controllerFolder);
        $this->controllerFolder = $fullPath;
        $this->recursive = $recursive;
    }

    /**
     * Get a list of all file paths in a directory that are PHP files.
     *
     * @param string|null $basePath internally used to recurse through a directory tree
     *
     * @return array
     */
    public function getFilePathsInDirectory( ?string $basePath = null ): array
    {
        $folderPath = realpath($this->controllerFolder . DIRECTORY_SEPARATOR . ($basePath ?? ''));
        $folderContent = scandir($folderPath);
        $folderContent = array_values(array_diff($folderContent, self::ILLEGAL_PATHS));

        $paths = [];

        foreach ( $folderContent as $item )
        {
            if ( is_dir($folderPath . DIRECTORY_SEPARATOR . $item) && $this->recursive )
            {
                array_push($paths, ...$this->getFilePathsInDirectory($basePath . DIRECTORY_SEPARATOR . $item));
            }
            else if ( preg_match('/^([\d|\D]*).php/', $item, $matches) )
            {
                $paths[] = realpath($folderPath . DIRECTORY_SEPARATOR . $item);
            }
        }

        return $paths;
    }

    /**
     * @inheritdoc
     */
    public function getControllerNamespaces(): array
    {
        $paths = $this->getFilePathsInDirectory();
        $namespaces = [];
        foreach ( $paths as $path )
        {
            //FIXME: there's probably a better way of handling this.
            $pathInfo = pathinfo($path);
            $path = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'];
            $relativePath = str_replace($this->controllerFolder . '/', '', $path);
            $relativePath = str_replace(DIRECTORY_SEPARATOR, '\\', $relativePath);
            $fullClassName = $this->rootNamespace . $relativePath;
            if ( !class_exists($fullClassName) )
                throw new BadNamespaceException($path, $fullClassName);
            if ( is_a($fullClassName, ControllerBase::class, true) )
                $namespaces[] = $fullClassName;
        }
        return $namespaces;
    }

}