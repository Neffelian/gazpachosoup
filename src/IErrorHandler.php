<?php

namespace WarpedDimension\GazpachoSoup;

use JetBrains\PhpStorm\ExpectedValues;
use Throwable;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Authentication\IAuthenticationHandler;

/**
 * Handle error events from the router.
 */
interface IErrorHandler
{

    /**
     * Handle a generic error from a route.
     *
     * @param Throwable $throwable
     *
     * @return Throwable|bool true = handled, false = default behaviour, throwable = new exception
     */
    function handle( Throwable $throwable ): Throwable|bool;

    /**
     * Handle rejection from an {@see IAuthenticationHandler}
     *
     * @param ControllerBase    $controller
     * @param RouteWrapper|null $route
     * @param Authenticated     $authenticationContext
     *
     * @return bool true = handled, false = default behaviour
     */
    function handleForbidden( ControllerBase $controller, ?RouteWrapper $route, Authenticated $authenticationContext ): bool;

    /**
     * Handle invalid method.
     *
     * @param ControllerBase    $controller
     * @param RouteWrapper|null $route
     * @param int               $method
     * @param int               $expectedMethod
     *
     * @return bool true = handled, false = default behaviour
     */
    function handleMethodNotAllowed( ControllerBase $controller, ?RouteWrapper $route, #[ExpectedValues(valuesFromClass: HttpVerbs::class)] int $method, #[ExpectedValues(valuesFromClass: HttpVerbs::class)] int $expectedMethod ): bool;

    /**
     * Handle page not found.
     *
     * @return bool true = handled, false = default behaviour
     */
    function handleNotFound(): bool;

}