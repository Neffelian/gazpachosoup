<?php

namespace WarpedDimension\GazpachoSoup\Authentication;

use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\RouteWrapper;

interface IAuthenticationHandler
{

    /**
     * Checks authentication on a route to ensure that it's okay to go here.
     *
     * @param ControllerBase     $controller            the controller that is being authenticated
     * @param ?RouteWrapper      $route                 metadata about the route (null if handling a controller level authentication)
     * @param Authenticated|null $authenticationContext context information from the {@see Authenticated} attribute.
     *
     * @return bool
     */
    function checkAuthentication( ControllerBase $controller, ?RouteWrapper $route, ?Authenticated $authenticationContext ): bool;

}