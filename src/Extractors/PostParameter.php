<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;

/**
 * Pass an entry in the {@see $_POST} array to the route in the specified parameter.
 *
 * @package WarpedDimension\GazpachoSoup\ParameterHandlers
 * @author  Ned Hyett <business@warped-dimension.com>
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class PostParameter implements IParameterExtractor
{

    /**
     * @var string|null The field to read from the {@see $_POST} array. If not null, overrides reading the parameter name instead.
     */
    public ?string $postField = null;

    /**
     * PostParameter constructor.
     *
     * @param string|null $postField an override for the post field, using this instead of the parameter name
     */
    public function __construct( ?string $postField = null )
    {
        $this->postField = $postField;
    }

    /**
     * @inheritdoc
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        if ( isset($_POST[ $this->postField ?? $funcParamName ]) )
            return $_POST[ $this->postField ?? $funcParamName ];
        return null;
    }

    /**
     * @inheritdoc
     */
    function isSet( string $funcParamName ): bool
    {
        return array_key_exists($this->postField ?? $funcParamName, $_POST);
    }

    /**
     * @inheritdoc
     */
    function getLocationName(): string
    {
        return 'Post Data';
    }
}