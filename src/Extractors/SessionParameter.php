<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;

/**
 * Pull a variable from the {@see $_SESSION} array.
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class SessionParameter implements IParameterExtractor
{

    /**
     * @var string|null The field to read from the {@see $_SESSION} array. If not null, overrides reading the parameter name instead.
     */
    public ?string $sessionField = null;

    /**
     * SessionParameter constructor.
     *
     * @param string|null $sessionField an override for the session field, using this instead of the parameter name
     */
    public function __construct( ?string $sessionField = null )
    {
        $this->sessionField = $sessionField;
    }

    /**
     * @inheritDoc
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        if ( isset($_SESSION[ $this->sessionField ?? $funcParamName ]) )
            return $_SESSION[ $this->sessionField ?? $funcParamName ];
        return null;
    }

    /**
     * @inheritDoc
     */
    function isSet( string $funcParamName ): bool
    {
        if ( !isset($_SESSION) )
            return false;
        return array_key_exists($this->sessionField ?? $funcParamName, $_SESSION);
    }

    /**
     * @inheritDoc
     */
    function getLocationName(): string
    {
        return 'Session';
    }
}