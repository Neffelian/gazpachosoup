<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;

/**
 * Passes the whole request body to the parameter.
 *
 * @package WarpedDimension\GazpachoSoup\Extractors
 * @author  Ned Hyett <business@warped-dimension.com>
 * @created 14/09/2021
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class RequestBodyParameter implements IParameterExtractor
{

    /**
     * @codeCoverageIgnore
     * @return string
     */
    protected static function getRequestBodyData(): string
    {
        return trim(file_get_contents('php://input'));
    }

    protected static function getRequestBody()
    {
        $data = static::getRequestBodyData();
        if ( strlen($data) === 0 )
            return null;
        $tempData = json_decode($data, true);
        if ( json_last_error() === JSON_ERROR_NONE )
            return $tempData;
        return $data;
    }

    /**
     * @inheritDoc
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        return static::getRequestBody();
    }

    /**
     * @inheritDoc
     */
    function isSet( string $funcParamName ): bool
    {
        return self::getRequestBody() !== null;
    }

    /**
     * @inheritDoc
     */
    function getLocationName(): string
    {
        return 'body';
    }

}