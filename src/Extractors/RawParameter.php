<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;

/**
 * If the raw body is an object, pull the top level keys out of it
 *
 * @package WarpedDimension\GazpachoSoup\Extractors
 * @author  Ned Hyett <business@warped-dimension.com>
 * @created 14/09/2021
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class RawParameter implements IParameterExtractor
{

    /**
     * @codeCoverageIgnore
     * @return string
     */
    protected static function getRequestBodyData(): string
    {
        return trim(file_get_contents('php://input'));
    }

    protected static function getRequestBody()
    {
        $data = static::getRequestBodyData();
        if ( strlen($data) === 0 )
            return false;
        $tempData = json_decode($data, true);
        if ( json_last_error() === JSON_ERROR_NONE )
            return $tempData;
        return false;
    }

    /**
     * @var string|null The field to read from the top level object. If not null, overrides reading the parameter name instead.
     */
    public ?string $rawField = null;

    /**
     * RawParameter constructor.
     *
     * @param string|null $rawField an override for the raw field, using this instead of the parameter name
     */
    public function __construct( ?string $rawField = null )
    {
        $this->rawField = $rawField;
    }

    /**
     * @inheritDoc
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        $body = self::getRequestBody();
        return $body[ $this->rawField ?? $funcParamName ];
    }

    /**
     * @inheritDoc
     */
    function isSet( string $funcParamName ): bool
    {
        $paramName = $this->rawField ?? $funcParamName;
        $body = self::getRequestBody();
        if ( !$body )
            return false;
        return array_key_exists($paramName, $body);
    }

    /**
     * @inheritDoc
     */
    function getLocationName(): string
    {
        return 'raw post';
    }
}