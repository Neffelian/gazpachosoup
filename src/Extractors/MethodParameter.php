<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;
use WarpedDimension\GazpachoSoup\HttpVerbs;

/**
 * Mark this parameter as an injection site for the request method as indicated by {@see HttpVerbs}
 *
 * @codeCoverageIgnore
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class MethodParameter
{

}