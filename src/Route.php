<?php

namespace WarpedDimension\GazpachoSoup;

use Attribute;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Describes a route in a {@see ControllerBase}
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Route
{

    /**
     * @var string regex of the path that should be taken. Starts with a /. Named capture groups can be mapped directly to parameter names.
     */
    public string $path;

    /**
     * @var int A bitmask of verbs that this route accepts.
     */
    #[ExpectedValues(flagsFromClass: HttpVerbs::class)]
    public int $method;

    public function __construct( string $path, #[ExpectedValues(flagsFromClass: HttpVerbs::class)] int $method = HttpVerbs::VERB_GET )
    {
        $this->path = $path;
        $this->method = $method;
    }

}