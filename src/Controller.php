<?php

namespace WarpedDimension\GazpachoSoup;

use Attribute;

/**
 * Provides common properties about the controller. Optional.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 * @codeCoverageIgnore
 */
#[Attribute(Attribute::TARGET_CLASS)]
class Controller
{

}