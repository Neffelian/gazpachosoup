<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

/**
 * Thrown when a required file was not found.
 * This is separate from a 404 as it is not describing web accessible content.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class FileNotFoundException extends GazpachoSoupException
{
    const CODE = 500;
    const HTTP_CODE = 500;
    const MESSAGE = 'A required file was not found at %s';
}