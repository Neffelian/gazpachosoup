<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

class BadParameterException extends GazpachoSoupException
{
    const CODE = 400;
    const HTTP_CODE = 400;
    const MESSAGE = 'Parameter \'%s\' is of wrong type \'%s\'. Expected type \'%s\' instead.';
}