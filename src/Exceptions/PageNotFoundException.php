<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

/**
 * Thrown when a request is made for a route that doesn't exist.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class PageNotFoundException extends GazpachoSoupException
{
    const CODE = 404;
    const HTTP_CODE = 404;
    const MESSAGE = 'Page not found';
}