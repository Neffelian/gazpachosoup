<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

use WarpedDimension\GazpachoSoup\HttpCodes;

class ClientForbiddenException extends GazpachoSoupException
{
    const CODE = HttpCodes::CODE_FORBIDDEN;
    const MESSAGE = "You are forbidden from accessing this resource.";
    const HTTP_CODE = HttpCodes::CODE_FORBIDDEN;
    const USER_MESSAGE = true;
}