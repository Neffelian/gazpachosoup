<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

/**
 * Thrown when the router encounters a request that is submitted with the wrong method.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class MethodNotAllowedException extends GazpachoSoupException
{
    const CODE = 405;
    const HTTP_CODE = 405;
    const MESSAGE = 'Method is not allowed for this resource';
}