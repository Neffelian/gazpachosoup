<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

use Exception;
use Throwable;
use WarpedDimension\GazpachoSoup\HttpCodes;

/**
 * Base exception for all GazpachoSoup exceptions.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class GazpachoSoupException extends Exception implements IRouterException
{

    /**
     * This exception code in the section.
     */
    const CODE = HttpCodes::CODE_SERVER_ERROR;

    /**
     * Section code. 0 = generic
     */
    const SECTION = 0;

    /**
     * Override to a valid HTTP code to override the HTTP response code
     */
    const HTTP_CODE = false;

    public static function getFullCode(): int
    {
        return static::CODE + static::SECTION;
    }

    /**
     * The message to print to the user, in {@see sprintf()} format.
     */
    const MESSAGE = 'Generic exception.';

    /**
     * If this exception is a user message, it won't be logged to PHP error out.
     * TODO: implement this.
     */
    const USER_MESSAGE = false;

    /**
     * SpiteException constructor.
     *
     * @param Throwable|null $previous
     * @param mixed          ...$interpolations
     */
    public function __construct( Throwable $previous = null, mixed ...$interpolations )
    {
        if ( static::SECTION % 1000 > 0 )
            die(sprintf('Bad exception section code (%u). Must be multiple of 1000.', static::SECTION));
        if ( static::CODE >= 1000 || static::CODE < 0 )
            die(sprintf('Bad exception code (%u). Must be between 0 and 999.', static::CODE));
        parent::__construct(sprintf(static::MESSAGE, ...$interpolations), static::getFullCode(), $previous);
    }

    function getHttpCode(): ?int
    {
        return static::getFullCode();
    }

    function isUserMessage(): bool
    {
        return static::USER_MESSAGE;
    }
}