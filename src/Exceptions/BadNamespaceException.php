<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

/**
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  ned
 * @created 04/08/2021
 */
class BadNamespaceException extends GazpachoSoupException
{
    const CODE = 500;
    const MESSAGE = "Controller file %s was not found in namespace %s. Are the namespace/controller folder paths mismatched?";

    public function __construct( string $file, string $namespace )
    {
        parent::__construct(null, $file, $namespace);
    }

}