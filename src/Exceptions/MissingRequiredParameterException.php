<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

/**
 * Thrown when a required parameter for a request is missing.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class MissingRequiredParameterException extends GazpachoSoupException
{
    const CODE = 400;
    const HTTP_CODE = 400;
    const MESSAGE = 'Missing required parameter (%s) in %s or is wrong type.';
}