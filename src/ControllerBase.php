<?php

namespace WarpedDimension\GazpachoSoup;

use Exception;
use ReflectionAttribute;
use ReflectionClass;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Extractors\IParameterExtractor;

/**
 * Base class for all controllers.
 *
 * TODO: allow controllers to specify route parameters.
 * TODO: allow controllers to be injected with parameters from values computed by {@see IParameterExtractor}
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
abstract class ControllerBase
{

    use ControllerResponseTrait;

    private Router $router;

    /**
     * @return Router
     * @codeCoverageIgnore
     */
    public function getRouter(): Router
    {
        return $this->router;
    }

    /**
     * Dynamically disable and enable this controller. Override in child class.
     *
     * @return bool
     * @codeCoverageIgnore
     */
    public static function enabled(): bool
    {
        return true;
    }

    /**
     * Get all routes from this controller.
     *
     * @return RouteWrapper[]
     */
    public final function getAllRoutes(): array
    {
        $rc = new ReflectionClass(static::class);
        $routes = [];
        foreach ( $rc->getMethods() as $method )
        {
            if ( count($method->getAttributes(Route::class, ReflectionAttribute::IS_INSTANCEOF)) === 0 )
                continue;

            if ( $method->isStatic() )
            {
                error_log(sprintf('Warning: method %s in class %s is marked with the Route attribute, but is marked as static. Skipping.', $method->getName(), $rc->getName()));
                continue;
            }
            $routes[] = new RouteWrapper($this, $method);
        }
        return $routes;
    }

    /**
     * Get the metadata attribute.
     *
     * @return Controller|null
     * @throws Exception
     */
    public final function getMetadataAttribute(): ?Controller
    {
        $rc = new ReflectionClass(static::class);
        $attrs = $rc->getAttributes(Controller::class, ReflectionAttribute::IS_INSTANCEOF);
        if ( count($attrs) === 0 )
            return null;
        $inst = $attrs[0]->newInstance();
        if ( $inst instanceof Controller )
            return $inst;
        // @codeCoverageIgnoreStart
        throw new Exception(sprintf('Metadata attribute was somehow not of correct type?!? This should never happen. (expected %s, got %s)', Controller::class, gettype($inst)));
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get the route attribute.
     *
     * @return Route|null
     * @throws Exception
     */
    public final function getRouteAttribute(): ?Route
    {
        $rc = new ReflectionClass(static::class);
        $attrs = $rc->getAttributes(Route::class, ReflectionAttribute::IS_INSTANCEOF);
        if ( count($attrs) === 0 )
            return null;
        $inst = $attrs[0]->newInstance();
        if ( $inst instanceof Route )
            return $inst;
        // @codeCoverageIgnoreStart
        throw new Exception(sprintf('Metadata attribute was somehow not of correct type?!? This should never happen. (expected %s, got %s)', Route::class, gettype($inst)));
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get the route attribute.
     *
     * @return Route|null
     * @throws Exception
     */
    public final function getAuthenticatedAttribute(): ?Authenticated
    {
        $rc = new ReflectionClass(static::class);
        $attrs = $rc->getAttributes(Authenticated::class, ReflectionAttribute::IS_INSTANCEOF);
        if ( count($attrs) === 0 )
            return null;
        $inst = $attrs[0]->newInstance();
        if ( $inst instanceof Authenticated )
            return $inst;
        // @codeCoverageIgnoreStart
        throw new Exception(sprintf('Metadata attribute was somehow not of correct type?!? This should never happen. (expected %s, got %s)', Authenticated::class, gettype($inst)));
        // @codeCoverageIgnoreEnd
    }

}