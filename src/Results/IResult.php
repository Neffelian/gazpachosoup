<?php

namespace WarpedDimension\GazpachoSoup\Results;

/**
 * Defines a result that can be sent out of a route.
 *
 * @package WarpedDimension\GazpachoSoup\Results
 * @author  Ned Hyett <business@warped-dimension.com>
 */
interface IResult
{

    function write( bool $close = false );

    function getContentType(): string;

    function getHeaders(): array;

    function getHeader( string $name ): mixed;

    function getHttpCode(): int;

    function getCode(): int;

    function getBody(): mixed;

}