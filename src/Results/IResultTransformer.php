<?php

namespace WarpedDimension\GazpachoSoup\Results;

/**
 * Transform an {@see IResult} to add/remove properties from it.
 *
 * @package WarpedDimension\GazpachoSoup\ResponseInterceptors
 * @author  Ned Hyett <business@warped-dimension.com>
 * @created 10/08/2021
 */
interface IResultTransformer
{

    function transform( IMutableResult $result ): IResult;

}