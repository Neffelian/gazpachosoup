<?php

namespace WarpedDimension\GazpachoSoup\Results;

use JsonSerializable;
use WarpedDimension\GazpachoSoup\ContentTypes;

/**
 * A result as part of a standardised JSON API.
 */
class JsonAPIResult implements IMutableResult, JsonSerializable
{

    /**
     * @var int|null the HTTP code to return.
     */
    private ?int $httpCode = null;

    /**
     * @var int the internal message code.
     */
    public int $code;

    /**
     * @var bool flag that this message is an error.
     */
    public bool $isError;

    /**
     * @var string|null textual message explaining the response.
     */
    public ?string $message;

    /**
     * @var array|null any top-level fields that should be added outside of the body.
     */
    public ?array $headers;

    /**
     * @var array|null The body of the response.
     */
    public ?array $body;

    /**
     * JsonAPIResult constructor.
     *
     * @param int         $code
     * @param array|null  $body
     * @param array|null  $headers
     * @param string|null $message
     * @param bool        $isError
     */
    public function __construct( int $code, ?array $body = null, ?array $headers = null, ?string $message = null, bool $isError = false )
    {
        $this->code = $code;
        $this->isError = $isError;
        $this->message = $message;
        $this->headers = $headers;
        $this->body = $body;
    }

    /**
     * Writes this JsonAPIResult out to the client.
     *
     * @param bool $close
     */
    public function write( bool $close = false )
    {
        if ( $this->httpCode !== null && !headers_sent() )
            header(sprintf('HTTP/%s %u %s', $_SERVER['SERVER_PROTOCOL'], $this->httpCode, $this->message));
        if ( $close )
            // @codeCoverageIgnoreStart
            exit(json_encode($this));
        // @codeCoverageIgnoreEnd
        else
            echo json_encode($this);
    }

    /**
     * Internal function used to force a return HTTP code.
     *
     * @param int $code
     *
     * @return static
     */
    public function setHttpCode( int $code ): static
    {
        $this->httpCode = $code;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $array = [
            'code' => $this->code,
            'error' => $this->isError,
            'message' => $this->message
        ];
        if ( $this->body !== null )
            $array['body'] = $this->body;
        if ( $this->headers !== null )
            $array = array_merge($array, $this->headers);
        return $array;
    }

    function getContentType(): string
    {
        return ContentTypes::Json;
    }

    function setCode( int $code ): static
    {
        $this->code = $code;
        return $this;
    }

    function setHeader( string $name, mixed $value ): static
    {
        $this->headers[ $name ] = $value;
        return $this;
    }

    function removeHeader( string $name ): static
    {
        unset($this->headers[ $name ]);
        return $this;
    }

    function clearHeaders(): static
    {
        $this->headers = [];
        return $this;
    }

    function setBody( mixed $body ): static
    {
        $this->body = $body;
        return $this;
    }

    function getHeaders(): array
    {
        return $this->headers;
    }

    function getHttpCode(): int
    {
        return $this->httpCode;
    }

    function getCode(): int
    {
        return $this->code;
    }

    function getBody(): mixed
    {
        return $this->body;
    }

    function getHeader( string $name ): mixed
    {
        return $this->headers[ $name ];
    }
}