<?php

namespace WarpedDimension\GazpachoSoup;

/**
 * List of content types
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 * @codeCoverageIgnore
 */
abstract class ContentTypes
{

    const Plain = 'text/plain';
    const Html = 'text/html';
    const Json = 'application/json';
    const Xml = 'text/xml';

}