<?php

namespace WarpedDimension\GazpachoSoup\ModelParser;

use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;
use WarpedDimension\GazpachoSoup\ModelParser\DefaultOverrides\DateTimeOverride;

class HandlerOverrideRegistry
{

    /**
     * @var array<class-string,ReflectionMethod>
     */
    private static array $handlers = [];

    /**
     * @param class-string $class
     *
     * @throws ReflectionException
     */
    public static function registerHandlersInClass( string $class, bool $overwrite = false )
    {
        $rc = new ReflectionClass($class);
        $methods = array_filter($rc->getMethods(ReflectionMethod::IS_STATIC), function( ReflectionMethod $method ) use ( $overwrite ): bool {
            if ( count($method->getAttributes(HandlerOverride::class)) === 0 )
                return false;
            /** @var HandlerOverride $meta */
            $meta = $method->getAttributes(HandlerOverride::class)[0]->newInstance();
            if ( array_key_exists($meta->getTargetClassName(), self::$handlers) && !$overwrite )
                return false;
            return true;
        });
        foreach ( $methods as $method )
        {
            /** @var HandlerOverride $meta */
            $meta = $method->getAttributes(HandlerOverride::class)[0]->newInstance();
            self::$handlers[ $meta->getTargetClassName() ] = $method;
        }
    }

    /**
     * @param class-string $class
     *
     * @return bool
     */
    public static function hasHandler( string $class ): bool
    {
        return array_key_exists($class, self::$handlers);
    }

    /**
     * @param class-string        $class
     * @param ReflectionParameter $parameter
     * @param mixed               $value
     *
     * @return mixed
     * @throws NoRegisteredHandlerOverrideException
     * @throws ReflectionException
     */
    public static function attemptConversion( string $class, ReflectionParameter $parameter, mixed $value ): mixed
    {
        if ( !self::hasHandler($class) )
            throw new NoRegisteredHandlerOverrideException($class);
        $handler = self::$handlers[ $class ];
        return $handler->invoke(null, $parameter, $value);
    }

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}

//Basic Override Registrations
HandlerOverrideRegistry::registerHandlersInClass(DateTimeOverride::class);