<?php

namespace WarpedDimension\GazpachoSoup\ModelParser\DefaultOverrides;

use Attribute;
use DateTime;
use DateTimeZone;
use Exception;
use ReflectionParameter;
use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverride;

#[Attribute(Attribute::TARGET_PARAMETER)]
class DateTimeOverride
{

    private string $timezone;

    /**
     * @param string $timezone
     */
    public function __construct( string $timezone )
    {
        $this->timezone = $timezone;
    }

    /**
     * @param ReflectionParameter $parameter
     * @param mixed               $value
     *
     * @return mixed
     * @throws Exception
     */
    #[HandlerOverride(DateTime::class)]
    public static function convertDateTime( ReflectionParameter $parameter, mixed $value ): mixed
    {
        $attrs = $parameter->getAttributes(DateTimeOverride::class);
        if ( count($attrs) > 0 )
            $param = $attrs[0]->newInstance();
        else
            $param = null;
        /** @var DateTimeOverride|null $param */

        if ( is_numeric($value) )
            return DateTime::createFromFormat('U', $value, $param !== null ? new DateTimeZone($param->timezone) : null);

        try
        {
            return new DateTime($value, $param !== null ? new DateTimeZone($param->timezone) : null);
        }
        catch ( Exception $ex )
        {
            error_log($ex->getMessage());
            error_log($ex->getTraceAsString());
            throw new Exception('Invalid DateTime string.', previous: $ex);
        }
    }

}