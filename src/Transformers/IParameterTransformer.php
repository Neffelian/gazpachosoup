<?php

namespace WarpedDimension\GazpachoSoup\Transformers;

use ReflectionParameter;

/**
 * Defines a transformer that receives the raw extracted value and returns a transformed version.
 */
interface IParameterTransformer
{

    const MODE_PRE = 0;
    const MODE_POST = 1;

    /**
     * Preprocess the raw value that has just been extracted.
     *
     * @param ReflectionParameter $parameter
     * @param mixed               $input
     *
     * @return mixed
     */
    function transformPre( ReflectionParameter $parameter, mixed $input ): mixed;

    /**
     * Preprocess the transformed value after the IRequestModel conversions have completed.
     *
     * @param ReflectionParameter $parameter
     * @param mixed               $input
     *
     * @return mixed
     */
    function transformPost( ReflectionParameter $parameter, mixed $input ): mixed;

}