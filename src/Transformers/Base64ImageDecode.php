<?php

namespace WarpedDimension\GazpachoSoup\Transformers;

use Attribute;
use ReflectionParameter;

/**
 * Automatically transforms a base64 image into a GDImage.
 *
 * @package WarpedDimension\GazpachoSoup\ParameterTransformer
 * @author  ned
 * @created 03/08/2021
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class Base64ImageDecode implements IParameterTransformer
{

    /**
     * @inheritDoc
     */
    function transformPre( ReflectionParameter $parameter, mixed $input ): mixed
    {
        preg_match('/data:(.*);base64(.*)/', trim($input), $matches);
        $data = str_replace(' ', '+', $matches[2]);
        $data = base64_decode($data);
        return @imagecreatefromstring($data);
    }

    /**
     * @inheritDoc
     */
    function transformPost( ReflectionParameter $parameter, mixed $input ): mixed
    {
        return $input;
    }

}