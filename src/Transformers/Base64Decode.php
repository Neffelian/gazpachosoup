<?php

namespace WarpedDimension\GazpachoSoup\Transformers;

use Attribute;
use ReflectionParameter;

#[Attribute(Attribute::TARGET_PARAMETER)]
class Base64Decode implements IParameterTransformer
{

    /**
     * @inheritDoc
     */
    function transformPre( ReflectionParameter $parameter, mixed $input ): mixed
    {
        return base64_decode($input);
    }

    /**
     * @inheritDoc
     */
    function transformPost( ReflectionParameter $parameter, mixed $input ): mixed
    {
        return $input;
    }
}